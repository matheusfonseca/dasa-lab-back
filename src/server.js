require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const laboratoryController = require('./controllers/laboratory')
const examController = require('./controllers/exam')

const app = express()

app.use(bodyParser.json())
app.use(cors())

app.use('/laboratory', laboratoryController())
app.use('/exam', examController())

module.exports = app
