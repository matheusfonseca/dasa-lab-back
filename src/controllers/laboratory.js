const { Router } = require('express')

const labRepo = require('../repositories/laboratory')

const { addLaboratory, getLaboratory, findLaboratory, findLaboratoryByText, updateLaboratory, removeLaboratory } = labRepo()

const laboratoryController = () => {
  const findByText = async (req, res) => {
    try {
      const labName = req.query.name
      const result = await findLaboratoryByText(labName)
  
      res.send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }
  const find = async (req, res) => {
    try {
      const query = req.query
      const result = await findLaboratory({ ...query })
  
      res.send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const get = async (req, res) => {
    try {
      const id = req.params.id
      const result = await getLaboratory(id)
  
      res.send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const create = async (req, res) => {
    try {
      const newLab = req.body
      const result = await addLaboratory(newLab)
  
      res.status(201).send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const update = async (req, res) => {
    try {
      const updatedLab = {
        _id: req.params.id,
        ...req.body
      }
      const result = await updateLaboratory(updatedLab)
  
      res.status(200).send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const remove = async (req, res) => {
    try {
      const id = req.params.id
      const result = await removeLaboratory(id)
  
      res.send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const router = Router()

  router.get('/', find)
  router.get('/text/', findByText)
  router.get('/:id', get)
  router.post('/', create)
  router.put('/:id', update)
  router.delete('/:id', remove)

  return router
}

module.exports = laboratoryController
