const { Router } = require('express')

const examRepo = require('../repositories/exam')

const { addExam, getExam, findExam, updateExam, removeExam } = examRepo()

const examController = () => {
  const findByText = async (req, res) => {
    try {
      const examName = req.query.name
      const result = await findExamByText(examName)
  
      res.send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const find = async (req, res) => {
    try {
      const query = req.query
      const result = await findExam({ ...query })
  
      res.send(result)
    } catch (err) {
      console.log(err)
      res.status(400).send(err)
    }
  }

  const get = async (req, res) => {
    try {
      const id = req.params.id
      const result = await getExam(id)
  
      res.send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const create = async (req, res) => {
    try {
      const newLab = req.body
      const result = await addExam(newLab)
  
      res.status(201).send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const update = async (req, res) => {
    try {
      const updatedExam = {
        _id: req.params.id,
        ...req.body
      }
      const result = await updateExam(updatedExam)
  
      res.status(200).send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const remove = async (req, res) => {
    try {
      const id = req.params.id
      const result = await removeExam(id)
  
      res.send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const router = Router()

  router.get('/', find)
  router.get('/text/', findByText)
  router.get('/:id', get)
  router.post('/', create)
  router.put('/:id', update)
  router.delete('/:id', remove)

  return router
}

module.exports = examController
