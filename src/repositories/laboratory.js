const Laboratory = require('../models/laboratory')

const laboratoryRepo = () => {
  const findLaboratoryByText = (partialText) => {
    console.log(partialText)
    return Laboratory.find({ name: { $regex: `${partialText}`, $options: "i" } }, function(err, docs) {
      console.log(err)
      return docs
      });
  }
  const addLaboratory = async (labInfo) => {
    const laboratory = new Laboratory(labInfo)
    try {
      const result = await laboratory.save()

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
    
  }

  const findLaboratory = async (params) => {
    try {
      const result = await Laboratory.find(params)

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  const getLaboratory = async (id) => {
    try {
      const result = await Laboratory.findById(id)

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }
  

  const updateLaboratory = async (lab) => {
    try {
      const result = await Laboratory.findByIdAndUpdate(lab.id, lab, { new: true })

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  const removeLaboratory = async (labId) => {
    try {
      const result = await Laboratory.findByIdAndUpdate(labId, { "active": false }, { new: true })

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }
  return {
    addLaboratory,
    getLaboratory,
    findLaboratory,
    findLaboratoryByText,
    updateLaboratory,
    removeLaboratory
  }
}

module.exports = laboratoryRepo
