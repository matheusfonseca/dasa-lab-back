const Exam = require('../models/exam')

const examRepo = () => {
  const findExamByText = (partialText) => {
    console.log(partialText)
    return Exam.find({ name: { $regex: `${partialText}`, $options: "i" } }, function(err, docs) {
      console.log(err)
      return docs
      });
  }

  const addExam = (examInfo) => {
    const exam = new Exam(examInfo)

    try {
      const result = exam.save()

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  const findExam = async (params) => {
    try {
      const result = await Exam.find(params)

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  const addExams = async (examArray) => {
    try {
      const result = await Exam.insertMany(examArray)

      return result
    } catch (err) {
      console.log(err)
      throw err
    }

  }

  const getExam = async (id) => {
    try {
      const result = await Exam.findById(id)

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  const updateExam = async (exam) => {
    try {
      const result = await Exam.findByIdAndUpdate(exam.id, exam, { new: true })

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  const removeExam = async (examId) => {
    try {
      const result = await Exam.findByIdAndUpdate(examId, { "active": false }, { new: true })

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  return {
    addExam,
    addExams,
    getExam,
    findExam,
    findExamByText,
    updateExam,
    removeExam
  }
}

module.exports = examRepo
