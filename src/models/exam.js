const mongoose = require('../util/dbConnection')
const { Schema } = require('mongoose')

const examSchema = new Schema({
  name:  {
    type: String,
    required: true
  },
  type: { 
    type: String, 
    required: true,
    enum: ['imagem', 'análise clinica'] 
  },
  active: { 
    type: Boolean, 
    default: true 
  },
  associatedLabs: { 
    type: [Array],
    default: []
  }
})

const examModel = mongoose.model('Exam', examSchema)

module.exports = examModel
