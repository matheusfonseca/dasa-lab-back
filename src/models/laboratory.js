const mongoose = require('../util/dbConnection')
const { Schema } = require('mongoose')

const laboratorySchema = new Schema({
  name:  {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  active: { 
    type: Boolean, 
    default: true 
  }
})

const laboratoryModel = mongoose.model('Laboratory', laboratorySchema)

module.exports = laboratoryModel
