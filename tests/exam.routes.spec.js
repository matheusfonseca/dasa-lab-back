const request = require('supertest')
const app = require('../src/server')
let cache = {}

describe('Exam process', () => {
    test('should create a new exam', async () => {
      const res = await request(app)
        .post('/exam')
        .send({
          name: "colesterol",
          type: "imagem"
        })
      cache.testId = res.body._id
      expect(res.statusCode).toEqual(201 || 200)
    })

    test('should get previously created exam', async () => {
      const res = await request(app)
        .get(`/exam/${cache.testId}`)

      expect(res.body._id).toEqual(cache.testId)
    })

    test('should edit previously created exam', async () => {
      cache.editedExam = {
        id: cache.testId,
        type: "contraste"
      }
      const res = await request(app)
        .put(`/exam/${cache.testId}`)
        .send(cache.editedExam)

      console.log(res.body)
      expect(res.body.type).toEqual(cache.editedExam.type)
    })

    test('should deactivate test exam', async () => {
      const res = await request(app)
        .delete(`/exam/${cache.testId}`)

      expect(res.body._id).toEqual(cache.testId)
      expect(res.body.active).toBeFalsy()
    })
})

