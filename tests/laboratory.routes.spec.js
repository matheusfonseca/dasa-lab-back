const request = require('supertest')
const app = require('../src/server')
let cache = {}

describe('Laboratory process', () => {
  test('should create a new lab', async () => {
    const res = await request(app)
      .post('/laboratory')
      .send({
        name: 'testoni auriemo',
        address: 'rua teste',
      })
      cache.testId = res.body._id
      expect(res.statusCode).toEqual(201 || 200)
    })
    test('should get previously created lab', async () => {
      const res = await request(app)
        .get(`/laboratory/${cache.testId}`)

        expect(res.body._id).toEqual(cache.testId)
    })
    test('should edit previously created lab', async () => {
      cache.editedLab = {
        id: cache.testId,
        name: 'delbonioni testo'
      }
      const res = await request(app)
        .put(`/laboratory/${cache.testId}`)
        .send(cache.editedLab)

        expect(res.body._id).toEqual(cache.testId)
        expect(res.body.name).toEqual(cache.editedLab.name)
    })
    test('should deactivate test lab', async () => {
      const res = await request(app)
        .delete(`/laboratory/${cache.testId}`)

        expect(res.body._id).toEqual(cache.testId)
        expect(res.body.active).toBeFalsy()
    })
})

